requirejs.config({

    paths: {

        /** Components **/
        app: 'src/lib',

        almond: 'bower_components/almond/almond',
        jquery: 'bower_components/jquery/dist/jquery',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap'

    },

    shim : {
        "bootstrap" : { "deps" :['jquery'] }
    }

});
