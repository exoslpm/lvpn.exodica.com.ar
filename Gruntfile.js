/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>' + '\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage : "" %>' + '\n' +
        ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;' + '\n' +
        ' * License: <%= _.pluck(pkg.licenses, "type").join(", ") %> (<%= _.pluck(pkg.licenses, "url").join(", ") %>)' + '\n' +
        ' */\n\n'
    },
    update_json: {
        bower: {
            src: 'package.json',
            dest: 'bower.json',
            fields: [
                        'name',
                        'version',
                        'author',
                        'description',
                        'keywords',
                        'license'
                    ]
        }
    },
    clean: {
        dist: ['dist/*']
    },
    requirejs: {
      site: {
        options: {
            mainConfigFile: "requirejs.conf.js",
            include: ['app/main'],
            insertRequire: ['app/main'],
            name: 'bower_components/almond/almond',
            out: "dist/js/main.js",
            optimize: 'uglify2',
            wrap: false
        }
      }
    },
    copy: {
        fonts: {
            expand: true,
            flatten: true,
            nonull: true,
            src: [
                'bower_components/font-awesome/fonts/*'
            ],
            dest: 'dist/fonts',
            filter: 'isFile'
        },
        images: {
            expand: true,
            flatten: true,
            nonull: true,
            src: 'res/images/{*,**}',
            dest: 'dist/images'
        },
        node: {
            src: 'res/exodica',
            dest: 'dist/exodica'
        }
    },
    less: {
        site: {
            options: {
                paths: ['dist/css'],
                compress: true,
                banner: '<%= meta.banner %>',
                modifyVars:{
                    imgPath: "'/images'"
                }
            },
            files: {
                "dist/css/main.css": 'src/styles/main.less'
            }
        }
    },
    htmlmin: {                                     
        site: {                                    
            options: {          
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                removeIgnored: true,

            },
            files: {                                  
                'dist/index.html': 'src/html/index.html'   
            }
        }
    },
    jshint: {
        options: {
            reporter: require('jshint-stylish'),
            jshintrc: '.jshintrc'
        },
        site: {
            src: [
                'src/**/*.js',
                'src/**/*.js'
            ]
        }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-update-json');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  // Custom tasks
  grunt.registerTask('min', ['clean', 'less', 'htmlmin']); // polyfil for uglify
  grunt.registerTask('check', ['jshint']);
  grunt.registerTask('build', ['clean', 'min', 'requirejs', 'copy']);

  // Default task
  grunt.registerTask('default', ['check','update_json']);

};
